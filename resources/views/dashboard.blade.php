
<!DOCTYPE html>
<html lang="en">
<head>
    <title>DashBoard Ponpes Hidayatul maarif</title>

    @include('layout.css-header')


</head>
<body class="hold-transition sidebar-mini text-sm">
<div class="wrapper">
@include('layout.navbar')
<!-- content -->
@yield('konten')
<!-- footer-->
    @include('layout.footer')
</div>
<!-- ./wrapper -->

</body >
@include('layout.js')
</html>

